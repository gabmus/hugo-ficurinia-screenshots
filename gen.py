from os import listdir

with open('public/index.html', 'w') as fd:
    fd.write(f'''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>hugo-ficurinia screenshots</title>
</head>
<body>
    {''.join([
        f'<img src="screenshots/{f}">' for f in listdir('screenshots')
    ])}
</body>
</html>''')
